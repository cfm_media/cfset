
Inspired by Chris Herdt's ArraySet [https://bitbucket.org/cherdt/]


#Possible use cases#


	<cffunction name="set_intersection" access="public" returntype="any">

		<cfargument name="setA" type="any" required="true">
	
		<cfargument name="setB" type="any" required="true">
	
		<cfargument name="setType" type="string" default="list" required="false">
	
		<cfset var setA = createObject("component","thirdparty.#setType#set").init( setA )>
	
		<cfset var setB = createObject("component","thirdparty.#setType#set").init( setB )>
	
		<cfreturn setA.intersection(setB, setType ) />
	
	</cffunction>



#Example#


	<cfset output = set_intersection('A,B,C', 'C,D,E' , 'list' ) />

	<!--- output = "C" --->

	<cfset output = set_intersection(['A','B','C'], ['C','D','E'] , 'array' ) />

	<!--- output = ["C"] --->




